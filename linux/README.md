### Overview

This document focusses on a Raspberry Pi based node app, though it is possible to host the app on
any computer.  If you already have a Linux machine set up, you can skip to [Configure Appliance to
Host Nodejs App](configure-appliance-to-host-nodejs-app).

This solution requires no appliance ethernet configuration, but note that there is an assumption the
appliance will be provisioned by a local DHCP server.

The capacity of the SD card I used to build the appliance is 32GB.  Turns out that 4GB will more
than suffice, and while I have not rebuilt using a 4GB card, I have downsized the partition to
reduce excessive pi boot time that occurs after a hard power cycle which results from file system
check on the way up.  My experience with a 4GB partition has been outstanding, with hard power
cycles resulting in boots in less than 2 minutes.  Probably best to start with a 4GB SD.  You can
start with a larger card and repartition, but note this operation can only be done when the SD is
unmounted.  I was able to boot to a GParted Live USB stick on my laptop and then shrink the SD
partition.

You will need to be able to edit text files in Linux.  I use vim, but beginners should stick with
nano.

### Appliance Requirements

Below are the components of the pi based network appliance.  The links in parens indicate the
products that I used.

1. Raspberry Pi 3 (https://www.amazon.com/gp/product/B01DMFQZXK/)
2. Micro SD card (https://www.amazon.com/gp/product/B010Q57T02/)

### Procedural Overview

1. Headless Pi Setup
2. Configure Appliance to Host Nodejs App
3. Configure Connector app

### Headless Pi Setup

The following are headless Debian installation procedures.  I implemented this from a Windows 10
bash console (http://www.windowscentral.com/how-install-bash-shell-command-line-windows-10), but you
could equally well use putty.  There are many headed and headless Debian installation guides
available on the Internet.

1.  [Download and unzip image](http://downloads.raspberrypi.org/raspbian_lite_latest).
2.  [Download and install image writer](https://etcher.io/).
3.  Write image to SD card.
4.  Create empty file named "ssh" in SD drive (e.g. create "e:\ssh").
5.  Plug in ethernet cable and boot pi
6.  Log onto home router to determine IP address supplied via DHCP
7.  establsh ssh session (e.g. "ssh pi@xxx.xxx.xxx.xxx", password is "raspberry") and type the following commands:

```
sudo su  
apt-get upgrade  
apt-get update  
```

If you are going to use wifi instead of ethernet, then append the following to the end of
/etc/wpa_supplicant/wpa_supplicant.conf:  

```
network={
    ssid="YourCaseSensitiveSSID"
    psk="YourWifiPassword"
}
```

### Configure Appliance to Host Nodejs App

Establish an ssh session as per the last section and type the following commands:

```
sudo su
curl -sL https://deb.nodesource.com/setup_10.x | sudo -E bash -
apt-get install -y nodejs
apt-get install -y git
```
### Configure Connector app on Linux

Type the following in an ssh session:

```
sudo su
git clone https://gitlab.com/JJNorcal/sp-pvo-con2.git
cd sp-mon-con2
npm install
```

Next, review and modify the files in the config directory.  They should be self explanatory.

At this point, you can test the app by temporarily modifying config/control.js to make both "basic"
and "toConsole" true, and then from sp-pvo-con2:

```
node app/main.js.
```

You should see output along these lines:

```
04/23/2017 14:32:19 success: 20170423 14:20 6258
04/23/2017 14:38:42 success: 20170423 14:25 5942
04/23/2017 14:43:42 success: 20170423 14:30 6356
```

Now reedit config/control.js as per your liking.

The last step is to configure your appliance to automatically launch the app on startup.  To do this,
you need to copy the sp-pvo-con2 file from your sp-pvo-con2/linux directory to the linux startup directory.
Assuming you just established an ssh session:

```
cd sp-pvo-con2/linux
cp sp-pvo-con2 /etc/init.d
```

The sp-pvo-con2 file you just copied assumes that you are logging in as pi.  If you have changed
this, then search for and modify SCRIPT and RUNAS.

Now you can reboot, ssh in, and confirm that the app has been automatically started by typing
"ps a | grep node".  You should see (the numbers in the first column correspond to process ID and can
be different on each boot).:

```
 1080 pts/0    Sl     0:03 /usr/bin/node /home/pi/sp-pvo-con2/app/main.js
 1239 pts/0    S+     0:00 grep node
```
