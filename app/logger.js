"use strict";

var control = require("../config/control.js");

(function main() {

    if (control.logging.basic) {
        return;
    }

    const util    = require("util");
    const winston = require("winston");
    const logger  = new winston.Logger();
    const sprintf = require("qprintf").sprintf;

    require("winston-daily-rotate-file");

    // Override the built-in console methods with winston hooks
    if (control.logging.toFile) {
        logger.add(winston.transports.DailyRotateFile, {
            filename: control.logging.filename,
            handleExceptions: true,
            exitOnError: false,
            level: "info",
            maxsize: 10 * 1024 * 1024,
            maxFiles: 2,
            json: false,
            timestamp: getTimeStamp
        });
    }

    if (control.logging.toConsole) {
        logger.add(winston.transports.Console, {
            colorize: true,
            timestamp: getTimeStamp,
            level: "info"
        });
    }

    function formatArgs(args){
        return [util.format.apply(util.format, Array.prototype.slice.call(args))];
    }

    console.log = function(){
        logger.info.apply(logger, formatArgs(arguments));
    };

    console.info = function(){
        logger.info.apply(logger, formatArgs(arguments));
    };

    console.warn = function(){
        logger.warn.apply(logger, formatArgs(arguments));
    };

    console.error = function(){
        logger.error.apply(logger, formatArgs(arguments));
    };

    console.debug = function(){
        logger.debug.apply(logger, formatArgs(arguments));
    };

    function getTimeStamp() {

        let now = new Date();
        let thisYear = now.getFullYear();
        let thisMonth = sprintf("%02d", now.getMonth() + 1);
        let thisDay = sprintf("%02d", now.getDate());
        let thisTime = now.toLocaleTimeString("en-US", { hour12: false } );

        return thisMonth + "/" + thisDay + "/" + thisYear + " " + thisTime;
    }
})();
