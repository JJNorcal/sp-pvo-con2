"use strict";

const fs = require("fs");

const maxHeaderSize = 32;

class PersistentArray {

    constructor(pathName) {

        let headerObj = { length: 0 };
        let headerString = "";

        let fd = fs.openSync(pathName, "a+");

        if (fd == -1) {
            return null;
        }

        this.pathName = pathName;
        this.fileSize = fs.statSync(pathName).size;
        this.length = 0;

        let position;
        let buffer = Buffer.alloc(maxHeaderSize);
        let bufString = "";

        for (position = 0; position < this.fileSize; position += headerString.length + headerObj.length) {

            fs.readSync(fd, buffer, 0, maxHeaderSize, position);

            bufString = buffer.toString();

            headerString = bufString.replace(/*{{*/ /([^}]*}).*/, "$1");
            headerObj = JSON.parse(headerString);

            ++this.length;
        }

        fs.closeSync(fd);

        if (this.length == 0) {
            fs.unlinkSync(pathName);
        }
    }

    push(obj) {

        let fd = fs.openSync(this.pathName, "a+");

        let jsonString = JSON.stringify(obj);
        let headerObj = { length: jsonString.length };

        let buffer = Buffer.from(JSON.stringify(headerObj));

        fs.writeSync(fd, buffer, 0, buffer.length, this.fileSize);
        this.fileSize += buffer.length;

        buffer = Buffer.from(jsonString);

        fs.writeSync(fd, buffer, 0, buffer.length, this.fileSize);
        this.fileSize += buffer.length;

        ++this.length;

        fs.closeSync(fd);
    }

    shift() {

        if (this.length == 0) {
            return;
        }

        let fd = fs.openSync(this.pathName, "r+");

        let buffer = Buffer.alloc(maxHeaderSize);

        fs.readSync(fd, buffer, 0, maxHeaderSize, 0);

        let bufString = buffer.toString();

        let headerString = bufString.replace(/*{{*/ /([^}]*}).*/, "$1");
        let headerObj = JSON.parse(headerString);

        let entryLength = headerObj.length + headerString.length;
        let newFileLength = this.fileSize - entryLength;

        buffer = Buffer.alloc(newFileLength);

        fs.readSync(fd, buffer, 0, newFileLength, entryLength);
        fs.writeSync(fd, buffer, 0, newFileLength, 0);
        fs.ftruncateSync(fd, newFileLength);

        this.fileSize = newFileLength;

        --this.length;

        fs.closeSync(fd);

        if (this.length == 0) {
            fs.unlinkSync(this.pathName);
        }
    }

    element(elementNumber) {

        if (this.fileSize == 0) {
            return undefined;
        }

        let fd = fs.openSync(this.pathName, "r+");

        let buffer = Buffer.alloc(maxHeaderSize);
        let obj = null;
        let headerObj = { length: 0 };
        let headerString;

        for (let position = 0; position < this.fileSize; position += headerString.length + headerObj.length) {

            fs.readSync(fd, buffer, 0, maxHeaderSize, position);

            let bufString = buffer.toString();

            headerString = bufString.replace(/*{{*/ /([^}]*}).*/, "$1");
            headerObj = JSON.parse(headerString);

            if (elementNumber-- > 0) {
                continue;
            }

            buffer = Buffer.alloc(headerObj.length);

            fs.readSync(fd, buffer, 0, headerObj.length, position + headerString.length);

            bufString = buffer.toString();

            obj = JSON.parse(bufString);

            break;
        }

        fs.closeSync(fd);

        return obj;
    }

    insert(elementNumber, obj) {

        if (elementNumber >= this.length) {
            this.push(obj);
            return;
        }

        let fd = fs.openSync(this.pathName, "r+");

        let buffer = Buffer.alloc(maxHeaderSize);

        let headerObj = { length: 0 };
        let runningLength = 0;
        let position;
        let headerString;

        for (position = 0; elementNumber-- > 0; position += headerString.length + headerObj.length) {

            fs.readSync(fd, buffer, 0, maxHeaderSize, position);

            let bufString = buffer.toString();

            headerString = bufString.replace(/*{{*/ /([^}]*}).*/, "$1");
            headerObj = JSON.parse(headerString);

            runningLength += headerObj.length + headerString.length;
        }

        let copyLength = this.fileSize - runningLength;

        buffer = Buffer.alloc(copyLength);

        fs.readSync(fd, buffer, 0, copyLength, position);

        let jsonString = JSON.stringify(obj);
        headerObj = { length: jsonString.length };

        let buffer2 = Buffer.from(JSON.stringify(headerObj));

        fs.writeSync(fd, buffer2, 0, buffer2.length, position);
        position += buffer2.length;

        buffer2 = Buffer.from(jsonString);

        fs.writeSync(fd, buffer2, 0, buffer2.length, position);
        position += buffer2.length;

        fs.writeSync(fd, buffer, 0, copyLength, position);

        this.fileSize = position + copyLength;

        ++this.length;

        fs.closeSync(fd);
    }

    delete(elementNumber) {

        if (this.fileSize == 0) {
            return undefined;
        }

        let fd = fs.openSync(this.pathName, "r+");

        let buffer = Buffer.alloc(maxHeaderSize);

        let headerObj = { length: 0 };
        let runningLength = 0;
        let headerString;

        for (let position = 0; position < this.fileSize; position += headerString.length + headerObj.length) {

            fs.readSync(fd, buffer, 0, maxHeaderSize, position);

            let bufString = buffer.toString();

            headerString = bufString.replace(/*{{*/ /([^}]*}).*/, "$1");
            headerObj = JSON.parse(headerString);

            let entryLength = headerObj.length + headerString.length;

            if (elementNumber-- > 0) {
                runningLength += entryLength;
                continue;
            }

            let copyLength = this.fileSize - runningLength - entryLength;

            buffer = Buffer.alloc(copyLength);

            fs.readSync(fd, buffer, 0, copyLength, position + entryLength);
            fs.writeSync(fd, buffer, 0, copyLength, position);
            fs.ftruncateSync(fd, position + copyLength);

            this.fileSize = runningLength + copyLength;

            --this.length;

            break;
        }

        fs.closeSync(fd);

        if (this.length == 0) {
            fs.unlinkSync(this.pathName);
        }
    }
}

module.exports = PersistentArray;
