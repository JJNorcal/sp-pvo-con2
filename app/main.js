"use strict";

const Credentials     = require("../config/credentials");
const Control         = require("../config/control");
const PersistentArray = require("./persistent-array");
const Request         = require("request-promise-native");
const Sprintf         = require("qprintf").sprintf;

require("./logger.js");

// global variables
var RetryQueue = new PersistentArray("./retry.dat");
let MissingDatapoints = new PersistentArray("./missing.dat");
let LastTime = new PersistentArray("./lastTime.dat");
let RetryTimer = null;
let RetryTimeout = 1000 * 12;
let LogonResponse = null;

// main
(async function Main() {

    console.log("starting connector");

    if (RetryQueue.length > 0) {
        RetryTimer = setTimeout(RetryTimeoutRoutine, RetryTimeout);
    }

    for ( ; ; ) {

        let startTime = GetStartTime();

        let endTime = GetEndTime();

        let token = await GetSunPowerToken();

        let energyData = await GetEnergyData(token, startTime, endTime);

        await ProcessEnergyData(energyData);

        await sleep(1000 * 60 * 15);
    }

})();

async function GetSunPowerToken() {

    if (LogonResponse) {
        return LogonResponse.Payload.TokenID;
    }

    for ( ; ; ) {

        LogonResponse = await LogonToSunPower();

        if (!LogonResponse) {
            console.warn("failed to obtain token");
            await sleep(1000 * 60 * 30);
            continue;
        }

        console.log("obtained new token");

        setTimeout(() => {
            LogonResponse = null;
        }, 1000 * 60 * LogonResponse.Payload.ExpiresInMinutes);

        return LogonResponse.Payload.TokenID;
    }
}

async function LogonToSunPower() {

    let logonRequest = {
        url: "https://monitor.us.sunpower.com/CustomerPortal/Auth/Auth.svc/Authenticate",
        method: "POST",
        headers: { "Content-Type": "application/json" },
        body: `${JSON.stringify(Credentials.sp)}`
    };

    let logonResponse = await SendRequest(logonRequest);

    if (logonResponse.statusCode != 200) {
        console.error(`logon failure: ${logonResponse.statusCode}`);
        return null;
    }

    return JSON.parse(logonResponse.body);
}

async function GetEnergyData(token, startTime, endTime) {

    let energyRequest = {
        url: `https://monitor.us.sunpower.com/CustomerPortal/SystemInfo/SystemInfo.svc/getEnergyData?endDateTime=${endTime}&guid=${token}&interval=minute&startDateTime=${startTime}`,
        method: "GET"
    };

    let energyResponse = await SendRequest(energyRequest);

    if (energyResponse.statusCode != 200) {
        console.error(`logon failure: ${energyResponse.statusCode}`);
        LogonResponse = null;
        return [];
    }

    let response = ParseJson(energyResponse.body);

    if (!response.Payload || !response.Payload.series || !response.Payload.series.data) {
        return [];
    }

    let data = response.Payload.series.data;

    console.log(`retrieved data: startTime= ${startTime}, endTime= ${endTime}, count= ${data.length}`);

    return data;
}

async function ProcessEnergyData(data) {

    if (data.length == 0) {
        return;
    }

    let dpIndex = 0;

    let twoWeeksAgo = new Date();

    twoWeeksAgo.setDate(twoWeeksAgo.getDate() - 14);

    for (let i = 0; i < MissingDatapoints.length; ++i) {

        let nextMissingDateString = MissingDatapoints.element(i);

        for ( ; dpIndex < data.length; ++dpIndex) {

            let dp = data[dpIndex];

            if (nextMissingDateString > dp.bdt) {
                continue;
            }

            if (nextMissingDateString < dp.bdt) {

                if (new Date(dp.bdt) > twoWeeksAgo) {
                    break;
                }

                console.error(`lost datapoint: ${dp.bdt}`);
            }

            else {
                await CommitData(dp);
            }

            MissingDatapoints.delete(i--);

            ++dpIndex;

            break;
        }
    }

    let lastDataPointTime = LastTime.element(0);
    let nextDate = null;

    if (lastDataPointTime) {

        while (dpIndex < data.length && data[dpIndex].bdt < lastDataPointTime) {
            ++dpIndex;
        }

        nextDate = new Date(lastDataPointTime);
        nextDate.setMinutes(nextDate.getMinutes() + 5);

        if (dpIndex < data.length && data[dpIndex].bdt == lastDataPointTime) {
            ++dpIndex;
        }
    }

    else if (dpIndex < data.length) {
        nextDate = new Date(data[dpIndex].bdt);
    }

    while (dpIndex < data.length) {

        let dp = data[dpIndex];
        let dpDate = new Date(dp.bdt);

        while (nextDate < dpDate && (dp.mp > 0 || (dpIndex > 0 && dp.mp > 0))) {

            let year = nextDate.getFullYear();
            let month = nextDate.getMonth();
            let day = nextDate.getDate();
            let hour = nextDate.getHours();
            let minute = nextDate.getMinutes();

            let missing = Sprintf("%d-%02d-%02dT%02d:%02d:00", year, month + 1, day, hour, minute);

            console.warn(`missing datapoint: ${missing}`);

            MissingDatapoints.push(missing);

            nextDate.setMinutes(nextDate.getMinutes() + 5);
        }

        await CommitData(dp);

        nextDate = dpDate;
        nextDate.setMinutes(nextDate.getMinutes() + 5);

        ++dpIndex;
    }

    LastTime.shift();
    LastTime.push(data[data.length - 1].bdt);
}

/*
{
    "bdt": "2017-07-20T14:00:00",
    "ep": 0.51,
    "eu": 0,
    "i": null,
    "mp": 6.1229
},
*/

async function CommitData(datapoint) {

    let dateTime = datapoint.bdt.split("T");
    let date = dateTime[0].replace(/-/g, "");
    let time = dateTime[1].replace(/:00$/, "");
    let power = Math.round(parseFloat(datapoint.mp) * 1000).toString();

    // pvo post template
    var pvoPost = {
        url: `https://pvoutput.org/service/r2/addstatus.jsp?d=${date}&t=${time}&v2=${power}`,
        method: "POST",
        headers: {
            "Host": "pvoutput.org",
            "X-Pvoutput-Apikey": Credentials.pvo.apiKey,
            "X-Pvoutput-SystemId": Credentials.pvo.systemId,
            "Accept": "*/*"
        }
    };

    var response = null;

    // process retry queue
    if (RetryQueue.length == 0) {
    
        if ((response = await SendRequest(pvoPost)).statusCode == 200) {

            // success
            console.log("success: " + date + " " + time + " " + power);     // success

            // PVO limits to 300 datapoints per hour
            if (Control.sendToPvo) {
                await sleep(1000 * 12);
            }

            return;
        }

        console.log("request: %j, response: %j", pvoPost, response);    // failure

        if (response.body && ! /Service Unavailable/i.test(response.body)) {
            return;
        }
    }

    RetryTimeout = 1000 * 60 * 5;

    RetryQueue.push(new RetryEntry(date, time, power, pvoPost));

    if (RetryTimer == null) {
        RetryTimer = setTimeout(RetryTimeoutRoutine, RetryTimeout);
    }
}

// synchronous http request wrapper
async function SendRequest(httpMessage) {

    if (!Control.sendToPvo && httpMessage.url.startsWith("https://pvoutput")) {
        return { "statusCode": 200, "body": "OK 200: Added Status" };
    }

    let retval;

    try {

        let promise = new Promise((resolve, reject) => {

            httpMessage.resolveWithFullResponse = true;

            Request(httpMessage)
            .then( (resp) => {
                resolve(resp);
            })
            .catch( (obj) => {
                reject(obj);
            });
        });

        promise.then(
            (obj) => {
                retval = obj;
            },
            (error) => {
                retval = error;
            }
        );

        await promise;
    }

    catch (err) {
        retval = err;
    }

    if (!retval) {
        retval = {};
    }

    if (!retval.statusCode) {
        retval.statusCode = 400;
    }

    return retval;
}


function GetStartTime() {

    if (MissingDatapoints.length > 0) {
        return MissingDatapoints.element(0);
    }

    let startDate;

    if (LastTime.length > 0) {
        startDate = new Date(LastTime.element(0));
        startDate.setMinutes(startDate.getMinutes() + 5);
    }

    else {
        startDate = new Date();
        startDate.setDate(startDate.getDate() - 14);
    }

    let year = startDate.getFullYear();
    let month = startDate.getMonth();
    let day = startDate.getDate();
    let hour = startDate.getHours();
    let minute = startDate.getMinutes();

    return Sprintf("%d-%02d-%02dT%02d:%02d:00", year, month + 1, day, hour, minute);
}

function GetEndTime() {

    let now = new Date();
    let year = now.getFullYear();
    let month = now.getMonth();
    let day = now.getDate();

    let tomorrow = new Date(year, month, day + 1);

    year = tomorrow.getFullYear();
    month = tomorrow.getMonth();
    day = tomorrow.getDate();

    return Sprintf("%d-%02d-%02dT00:00:00", year, month + 1, day);
}

function sleep(ms){
    return new Promise(resolve=>{
        setTimeout(resolve,ms);
    });
}

async function RetryTimeoutRoutine() {

    var response;

    console.log("retry attempt, queue length = " + RetryQueue.length.toString());

    var retry = RetryQueue.element(0);

    if ((response = await SendRequest(retry.post)).statusCode == 200) {

        console.log("success: " + retry.date + " " + retry.time + " " + retry.power);     // success

        RetryTimeout = 1000 * 12;

        RetryQueue.shift();
    }

    else {
        console.log("request: %j, response: %j", retry.post, response);    // failure
    }

    if (RetryQueue.length > 0) {
        RetryTimer = setTimeout(RetryTimeoutRoutine, RetryTimeout);
    }

    else {
        RetryTimer = null;
    }
}

class RetryEntry {

    constructor(date, time, power, pvoPost) {
        this.date = date;
        this.time = time;
        this.power = power;
        this.post = pvoPost;
    }
}

function ParseJson(jsonString) {

    let json = {};

    try {
        json = JSON.parse(jsonString);
    }

    catch (err) {
        console.error("JSON parsing error:", err);
    }

    return json;
}
