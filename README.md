### Intro

This project maintains a node app which implements a SunPower (SP) to PVOutput connector.  The
initial project focussed exclusively on a Raspberry Pi based appliance which hosts the app while
acting as a network switch, thus eliminating the need for port mirroring.  However, a SP monitoring
firmware update enabled https encryption, thus rendering this packet monitoring inoperative.

This fork from sp-pvo-con leverages SP's unpublished client API to obtain the power data from the SP
monitoring site.  One positive impact is that the app no longer needs to run on host physically
located between the monitoring equipment and the internet.  A negative impact is that SP can change
their underlying API at any time.

[Linux documentation](./linux/README.md) describes creation of a Raspberry Pi appliance image along
with installation of the connector app running as a Linux service.

[Windows documentation](./windows/README.md) describes installation of the connector app running as
a Windows service.
