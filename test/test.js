let assert = require("chai").assert;

class testObj {
    constructor(stringVal, numVal) {
        this.stringVal = stringVal;
        this.numVal = numVal;
    }
}

describe("persistent arrays", function () {

    let header = { length: 0 };
    let headerString;
    let obj;
    let exists;

    const fs = require("fs");

    let pathName = "./testArray.dat";

    if (fs.existsSync(pathName)) {
        fs.unlinkSync(pathName);
    }

    let PersistentArray = require("../app/persistent-array");
    let pArray = new PersistentArray(pathName);

    it("instantiate array", function () {

        let exists = fs.existsSync(pathName);

        assert(!exists, "file should not exist");
        assert(pArray.length == 0, "array length should be 0");
        assert(pArray.fileSize == 0, "file size should be 0");
    });

    let obj1 = new testObj("a string", 1);

    it("push object", function () {

        header.length = JSON.stringify(obj1).length;

        headerString = JSON.stringify(header);

        pArray.push(obj1);

        assert(pArray.length == 1, "array length should be 1");
        assert(pArray.fileSize == header.length + headerString.length);
    });

    it("examine element", function () {

        obj = pArray.element(0);

        assert(obj.stringVal == obj1.stringVal, "string examined should match string pushed");
        assert(obj.numVal == obj1.numVal, "number examined should match number pushed");
    });

    it("shift array", function () {

        pArray.shift();

        exists = fs.existsSync(pathName);

        assert(!exists, "file should be deleted");

        assert(pArray.length == 0, "array length should be 0");
        assert(pArray.fileSize == 0, "file size should be 0");
    });

    it("delete elements", function () {

        let testObjects = [
            new testObj("short string", 9),
            new testObj("a longer string", 123456789),
            new testObj("mary had a little lamb whose fleece was white as snow", 1234567891234567889),
            new testObj("another string", -1),
            new testObj("yet another string", -1234)
        ];

        let i;

        for (i = 0; i < testObjects.length; ++i) {
            pArray.push(testObjects[i]);
        }

        for (i = 0; i < testObjects.length; ++i) {

            obj = pArray.element(i);

            assert(obj.stringVal == testObjects[i].stringVal, "string examined should match string pushed");
            assert(obj.numVal == testObjects[i].numVal, "number examined should match number pushed");
        }

        // delete first
        testObjects.splice(0, 1);
        pArray.delete(0);

        for (i = 0; i < testObjects.length; ++i) {

            obj = pArray.element(i);

            assert(obj.stringVal == testObjects[i].stringVal, "string examined should match string pushed");
            assert(obj.numVal == testObjects[i].numVal, "number examined should match number pushed");
        }

        // delete last
        testObjects.splice(3, 1);
        pArray.delete(3);

        for (i = 0; i < testObjects.length; ++i) {

            obj = pArray.element(i);

            assert(obj.stringVal == testObjects[i].stringVal, "string examined should match string pushed");
            assert(obj.numVal == testObjects[i].numVal, "number examined should match number pushed");
        }

        // delete middle
        testObjects.splice(1, 1);
        pArray.delete(1);

        for (i = 0; i < testObjects.length; ++i) {

            obj = pArray.element(0);

            assert(obj.stringVal == testObjects[i].stringVal, "string examined should match string pushed");
            assert(obj.numVal == testObjects[i].numVal, "number examined should match number pushed");

            pArray.shift();
        }

        exists = fs.existsSync(pathName);

        assert(!exists, "file should be deleted");

        assert(pArray.length == 0, "array length should be 0");
        assert(pArray.fileSize == 0, "file size should be 0");
    });

    it("insert elements", function () {

        let testObjects = [
            new testObj("short string", 9),
            new testObj("a longer string", 123456789),
            new testObj("mary had a little lamb whose fleece was white as snow", 1234567891234567889),
            new testObj("another string", -1),
            new testObj("yet another string", -1234)
        ];

        pArray.insert(0, testObjects[1]);

        obj = pArray.element(0);

        assert(obj.stringVal == testObjects[1].stringVal, "string examined should match string pushed");
        assert(obj.numVal == testObjects[1].numVal, "number examined should match number pushed");

        pArray.insert(0, testObjects[0]);
        pArray.insert(3, testObjects[4]);
        pArray.insert(2, testObjects[2]);
        pArray.insert(3, testObjects[3]);

        for (let i = 0; i < testObjects.length; ++i) {

            obj = pArray.element(0);

            assert(obj.stringVal == testObjects[i].stringVal, "string examined should match string pushed");
            assert(obj.numVal == testObjects[i].numVal, "number examined should match number pushed");

            pArray.shift();
        }

        exists = fs.existsSync(pathName);

        assert(!exists, "file should be deleted");

        assert(pArray.length == 0, "array length should be 0");
        assert(pArray.fileSize == 0, "file size should be 0");
    });
});
