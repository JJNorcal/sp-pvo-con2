#!/bin/sh

shortUsage="usage: sppush StartTime EndTime SpUsername SpPassword PvoKey PvoSystemId"

usage=`cat <<!

$shortUsage

Pushes SunPower data from StartTime through EndTime.

Time format is YYYY-MM-DDTHH:MM:SS.

PvoKey is PVO API key.

PvoSystemId is corresponding PVO SystemId.

!`

if [ $# -ne 6 ]; then 
    echo "$usage"
    exit
fi

startDate=$1
endDate=$2
spUsername=$3
spPassword=$4
pvoKey=$5
pvoSystemId=$6

logon=$(curl -s -d '{"username": "'$spUsername'", "password": "'$spPassword'", "isPersistent": false}' -H "Content-Type: application/json" "https://monitor.us.sunpower.com/CustomerPortal/Auth/Auth.svc/Authenticate")

#logon='{"StatusCode":"200","ResponseMessage":"Success","Payload":{"ExpiresInMinutes":1439,"TokenID":"de61b2e7-682c-4c9e-a59c-45b965b3bab6"}}'

statusCode=$(echo $logon | sed -n -e 's/"//g' -e 's/.*StatusCode:\([0-9][0-9]*\).*$/\1/p')

if [ "$statusCode" != "200" ]; then
    echo "Failed to get SunPower token: $logon"
    exit
fi

token=$(echo $logon | sed -n -e 's/"//g' -e 's/.*TokenID:\([a-z0-9-][a-z0-9-]*\).*$/\1/p')

data=$(curl -s "https://monitor.us.sunpower.com/CustomerPortal/SystemInfo/SystemInfo.svc/getEnergyData?endDateTime=$endDate&guid=$token&interval=minute&startDateTime=$startDate")

#data='{"StatusCode":"200","ResponseMessage":"Success","Payload":{"errorMessage":null,"series":{"data":[{"bdt":"2018-07-26T13:00:00","ep":0.45,"eu":0,"i":null,"mp":5.4217},{"bdt":"2018-07-26T13:05:00","ep":0.45,"eu":0,"i":null,"mp":5.431},{"bdt":"2018-07-26T13:10:00","ep":0.46,"eu":0,"i":null,"mp":5.4423},{"bdt":"2018-07-26T13:15:00","ep":0.45,"eu":0,"i":null,"mp":5.5083}]},"xmin":3}}'

statusCode=$(echo $data | sed -n -e 's/"//g' -e 's/.*StatusCode:\([0-9][0-9]*\).*$/\1/p')

if [ "$statusCode" != "200" ]; then
    echo "Failed to get SunPower powerdata: $data"
    exit
fi

datapoints=$(echo $data | sed -e 's/"//g' -e 's/\].*//' -e 's/.*data:\[\(.*\)/\1/')

if [ "$datapoints" == "" ]; then
    echo "no data in range"
    exit
fi

while [ true ]; do

    dp=$(echo $datapoints | sed 's/\({[^}]*}\).*/\1/')

    datapoints=$(echo $datapoints | sed 's/{[^}]*},*\(.*\)/\1/')

    date=$(echo $dp | sed 's/.*bdt:\([T:0-9-]*\).*/\1/')
    day=$(echo $date | sed -e 's/-//g' -e 's/\(.*\)T.*/\1/')
    time=$(echo $date | sed 's/.*T\(..:..\).*/\1/')

    kWatts=$(echo $dp | sed 's/.*mp:\([0-9.]*\).*/\1/')
    watts=$(echo $kWatts | awk '{printf("%.0f", $0 * 1000)}')

    push=$(curl -s -d "d=$day" -d "t=$time" -d "v2=$watts" -H "X-Pvoutput-Apikey: $pvoKey" -H "X-Pvoutput-SystemId: $pvoSystemId" https://pvoutput.org/service/r2/addstatus.jsp)

    echo $day $time "$push"

    if [ "$datapoints" == "" ]; then
        break;
    fi

    sleep 12;

done
