module.exports = {

    // mandatory pvo credentials
    pvo: {
        apiKey   : "my-pvo-api-key",
        systemId : "my-pvo-system-id"
    },

    // mandatory sunpower credentials
    sp: {
        username :"my-sp-username",
        password :"my-sp-password"
    }
}
