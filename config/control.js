"use strict";

module.exports = {

    // false for testing purposes
    sendToPvo       : true,

    logging: {

        // true disables winston
        basic       : false,

        // true logs to console (for debugging)
        toConsole   : false,

        // true logs to file
        toFile      : true,

        // log file (will rotate daily)
        filename    : "/home/pi/sp-pvo-con2/logs/log"
    }
}
